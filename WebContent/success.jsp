<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" import="java.util.*" %>
 
 

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Web Service Shopping Cart</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
</head>

<body>

    <!-- Navigation -->
     <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">ABC</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#">Home </a></li>
		<li class="active"><a href="#">Men's Wrist watches<span class="sr-only">(current)</span></a></li>
		<li><a href="#">Women's wrist watches </a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Change Password</a></li>
            <li><a href="#">Change Username</a></li>
            <li><a href="#">Add card</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Link New Card</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Log out</a></li>
            
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Go</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ESTIO
                    <small>Pittsburgh Polo Club Brown Analog men Watch</small>
                    <font color="#5cb85c">(Order Successful)</font>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-4">
                <img class="img-responsive" src="${pageContext.request.contextPath}/images/sw5.jpg" alt="">
            </div>

            <div class="col-md-8">
                <h3 class="success_message">
                    Thank you for purchasing this product! Your order has been successfuly placed.

                    You will receive your item in registered post within 2-3 weeks.
                    <br/>
                </h3>

                <p>
                    ---------------------------------------------------------------------------------------------------------------------------------------------
                </p>

                <h4>
                    <br/>

                    For more information call our customer support:

                    <br/><br/>                        
                    
                    +94717877425
                </h4>

                
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    
    <footer>
	<div style=" position:absolute; bottom:0px; right:0px;left:0px;">
            <nav class="navbar navbar-inverse navbar-static-top">
                 <div class="container">
                   <span><h5 style="color:white;text-align:center; ">Copyright &copy 2016    ABC Company    all rights reserved</h5></span>
                 </div>
            </nav>
			</div>
    </footer>
    <!-- /.container -->
	

    <script src="${pageContext.request.contextPath}/jq/jquery-2.2.4.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>