<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" import="java.util.*" %>


<html>
  <head>
	<title>ABC company</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" /> 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
  </head>
  <body>

    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">ABC</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#">Home </a></li>
		<li class="active"><a href="#">Men's Wrist Watch <span class="sr-only">(current)</span></a></li>
		<li><a href="#">Women's wrist watches</a></li>
        
       
      </ul>
      <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Go</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



    
	
    <div class="container-fluid">
		<div class="row">
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw5.jpg" alt="...">
			  <div class="caption">
				<h3>ESOTO</h3>
				<p>ESOTO Black Leather Men Analog Watch</br></br>$50</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw4.jpg" alt="...">
			  <div class="caption">
				<h3>Pittsburgh</h3>
				<p>Pittsburgh Polo Club Brown Analog men Watch </br>$45</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw1.jpg" alt="...">
			  <div class="caption">
				<h3>Davidson</h3>
				<p>Davidson Silver Metal Analog Men Watch - ROSRA-007</br>$88</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw2.jpg" alt="...">
			  <div class="caption">
				<h3>Greenwich</h3>
				<p>
Greenwich Polo Club Black Leather Analog Men Watch</br>$75</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		</div>
		
<div class="row">
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw3.jpg" alt="...">
			  <div class="caption">
				<h3>MTG-Black</h3>
				<p>
MTG Black Stainless Steel Analog Men Watch </br>$45</br></p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw6.jpg" alt="...">
			  <div class="caption">
				<h3>Adine</h3>
				<p>
Adine Silver Metal Analog Men Watch Ad1009</br>$72</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw7.jpg" alt="...">
			  <div class="caption">
				<h3>Gold Fossil</h3>
				<p>Gold Fossil watch </br></br>$110</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-6 col-md-3">
			<div class="thumbnail">
			  <img src="images/sw8.jpg" alt="...">
			  <div class="caption">
				<h3>Polo Hunter</h3>
				<p>Polo Hunter Brown Leather Analog Men Watch </br>$100</p>
				<p><a href="#" class="btn btn-primary" role="button">Quick View</a> <a href="#" class="btn btn-default" role="button">Quick Buy</a></p>
			  </div>
			</div>
		  </div>
		</div>
		
	
	
	</div>

   
   




  <nav class="navbar navbar-inverse navbar-static-top">
       <div class="container">
      <span><h5 style="color:white;text-align:center; ">Copyright © 2016    ABC Company    all rights reserved</h5></span>
       </div>
    </nav>
  
  
  
 
  
  <script src="${pageContext.request.contextPath}/jq/jquery-2.2.4.min.js"></script>
 <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	
	
  </body>
</html>