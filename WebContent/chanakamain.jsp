<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.*"%>

<html>
<head>
<title>ABC Corperetion</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>

<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Brand</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a
						href="${pageContext.request.contextPath}/chanakamain.jsp">Home
							<span class="sr-only">(current)</span>
					</a></li>
					<li><a href="${pageContext.request.contextPath}/list.jsp">Men's
							Wrist watches </a></li>
					<li><a href="#">Women's wrist watches</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Personal Account <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Change Password</a></li>
							<li><a href="#">Change Username</a></li>
							<li><a href="#">Add card</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Link New Card</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Log out</a></li>
						</ul></li>
				</ul>

				<form class="navbar-form navbar-right" role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Go</button>
				</form>

			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->

	</nav>
	<!--=====================================================================================-->
	<div class="container">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
				<li data-target="#myCarousel" data-slide-to="4"></li>
				<li data-target="#myCarousel" data-slide-to="5"></li>
				<li data-target="#myCarousel" data-slide-to="6"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="${pageContext.request.contextPath}/images/w1.jpg"
						alt="Chania">
					<div class="carousel-caption">
						<h3>Chania</h3>
						<p>The atmosphere in Chania has a touch of Florence and
							Venice.</p>
					</div>
				</div>

				<div class="item">
					<img src="${pageContext.request.contextPath}/images/w2.jpg"
						alt="Chania">
					<div class="carousel-caption">
						<h3>Chania</h3>
						<p>The atmosphere in Chania has a touch of Florence and
							Venice.</p>
					</div>
				</div>

				<div class="item">
					<img src="${pageContext.request.contextPath}/images/w3.jpg"
						alt="Flower" height="1000px">
					<div class="carousel-caption">
						<h3>Flowers</h3>
						<p>Beatiful flowers in Kolymbari, Crete.</p>
					</div>
				</div>

				<div class="item">
					<img src="${pageContext.request.contextPath}/images/w4.jpg"
						alt="Flower">
					<div class="carousel-caption">
						<h3>Flowers</h3>
						<p>Beatiful flowers in Kolymbari, Crete.</p>
					</div>
				</div>
				<div class="item">
					<img src="${pageContext.request.contextPath}/images/w5.jpg"
						alt="Flower">
					<div class="carousel-caption">
						<h3>Flowers</h3>
						<p>Beatiful flowers in Kolymbari, Crete.</p>
					</div>
				</div>

				<div class="item">
					<img src="${pageContext.request.contextPath}/images/w7.jpg"
						alt="Flower">
					<div class="carousel-caption">
						<h3>Flowers</h3>
						<p>Beatiful flowers in Kolymbari, Crete.</p>
					</div>
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

	</div>




	<!--===========================================================-->
	<div class="container">
		<div class="jumbotron">
			<h1>ABC Company</h1>
			</br>
			<h3>Always The Best From Us.Keep In Touch.....</h3>
			</br>
			<p>
				33A, 5th Lane,</br> Colpetty</br> Colombo 03,</br> Sri Lanka</br> Tel.: 00 94 11 522 62
				32</br> Fax: 00 94 11 522 62 96
			</p>
			<p>
			<h2>
				<a class="btn btn-primary btn-lg"
					href="${pageContext.request.contextPath}/list.jsp" role="button">Visit</a>
			</h2>
			</p>
		</div>
	</div>

	<!----------------------------------------------------------------------------------------->

	<div class="container">
		<h1>
			<span class="label label-default">We Are Unique From Others</span>
		</h1>
	</div>



	<!--=======================================================================================-->
	<div class="container">
		<div class="row ">
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="${pageContext.request.contextPath}/images/vision.jpg"
						alt="...">
					<div class="caption">
						<h3>Our vision</h3>
						<p>provides quality products for people concerned about value.
							We endeavor to create elegant watches with function and utility
							for daily use.</p>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						<p>
							<a href="#" class="btn btn-primary" role="button">Read More</a>
						</p>
					</div>
				</div>
			</div>


			<div class="col-sm-6 col-md-4 ">
				<div class="thumbnail">
					<img src="${pageContext.request.contextPath}/images/love.jpg"
						alt="...">
					<div class="caption">
						<h3>Our Service</h3>
						<p>Beginning is traced back to 2006, when a professional
							pilot, also a designer, found inspiration to design watches while
							away from home and working his flight schedule. Like countless
							other pilots, he is drawn to the beautiful wristwatches of the
							world as both a fan of mechanical things, and an aficionado of
							artistic form. Intrigue and captivation exist in the miniature
							machines that are worn on the wrist.</p>
						<p>
							<a href="#" class="btn btn-primary" role="button">Read More</a>
						</p>
					</div>
				</div>
			</div>


			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="${pageContext.request.contextPath}/images/stand.jpg"
						alt="...">
					<div class="caption">
						<h3>Our Standards</h3>
						<p>
							Understands the importance of community involvement, including
							fostering the development of today's youth. We are proud to
							contribute a portion of proceeds to aviation and engineering
							youth development programs,</br> as well as local youth organizations.
						</p>
						</br>
						</br>
						</br>
						<p>
							<a href="#" class="btn btn-primary" role="button">Read More</a>
						</p>
					</div>
				</div>
			</div>


		</div>
	</div>

	<!---------------------------------------------------------------------------------------->

	<nav class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<span><h5 style="color: white; text-align: center;">Copyright
					© 2016 ABC Company all rights reserved</h5></span>
		</div>
	</nav>


	<script src="${pageContext.request.contextPath}/jq/jquery-2.2.4.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>